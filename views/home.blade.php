@extends('layouts.base')

@section('content')
  <h1 class="page-header">Search Cars</h1>
  <div class="container-fluid" ng-controller="searchCtrl">
    <div class="jumbotron">
      <div class="row form-inline">
        <div class="col-md-6">
          <div class="row">
            <label class="control-label col-sm-2">Zipcode</label>
            <div class="col-sm-10">
              <selectize config='zipcodeConfig' options='zipcodeOptions' ng-model="zipcode" class="form-control full-width"></selectize>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <label class="control-label col-sm-2">Distance (miles)</label>
            <div class="col-sm-10">
              <input type="number" class="form-control full-width" ng-model="distance" placeholder="Enter Distance"/>
            </div>
          </div>
        </div>
      </div>
      <div class="row top-spacing">
        <div class="col-md-2">
          <button class="btn btn-success" ng-click="fetchInventories()">Search</button>
        </div>
      </div>
    </div>
    <h2>Vehicles</h2>
    <table datatable="" dt-options="dtOptions" dt-columns="dtColumnDefs" dt-instance="dtInstance" class="table table-striped table-bordered">
    </table>
  </div>
@endsection

@section('custom_js')
  <script src="/assets/js/controllers/searchCtrl.js"></script>
  <script src="/assets/js/services/inventoryService.js"></script>
  <script src="/assets/js/services/zipcodeService.js"></script>
@endsection
