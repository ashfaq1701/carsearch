<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="icon" href="https://getbootstrap.com/docs/3.3/favicon.ico">
    <title>Car Search</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/selectize.bootstrap3.css" rel="stylesheet">
    <link href="/assets/css/angular-datatables.min.css" rel="stylesheet">
    <link href="/assets/css/datatables.bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/dashboard.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
    @yield('custom_css')
  </head>

  <body ng-app="carSearchApp">
    @include('partials.navbar')
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          @include('partials.sidebar')
        </div>
        @include('partials.content_area')
      </div>
    </div>
    <script src="/assets/js/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/jquery.dataTables.min.js"></script>
    <script src="/assets/js/selectize.min.js"></script>
    <script src="/assets/js/spin.min.js"></script>
    <script src="/assets/js/angular.min.js"></script>
    <script src="/assets/js/angular-datatables.js"></script>
    <script src="/assets/js/angular-datatables.bootstrap.min.js"></script>
    <script src="/assets/js/angular-selectize.js"></script>
    <script src="/assets/js/angular-spinner.min.js"></script>
    <script src="/assets/js/app.js"></script>
    @yield('custom_js')
  </body>
</html>
