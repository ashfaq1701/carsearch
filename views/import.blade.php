@extends('layouts.base')

@section('content')
  <h1 class="page-header">Import</h1>
  <div class="container-fluid" ng-controller="importCtrl">
    <span us-spinner spinner-key="spinner-1" spinner-on="processing"></span>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label class="control-label">Input CSV File</label>
          <input type="file" file-input ng-model="uploadFile" ng-change="fileUploaded()" class="form-control" ng-disabled="processing"/>
        </div>
      </div>
      <div class="col-md-12 padding-btm">
        <label class="radio-inline"><input type="radio" ng-model="uploadType" value="zipcode" ng-disabled="processing">Zipcode</label>
        <label class="radio-inline"><input type="radio" ng-model="uploadType" value="customer" ng-disabled="processing">Customer</label>
        <label class="radio-inline"><input type="radio" ng-model="uploadType" value="inventory" ng-disabled="processing">Inventory</label>
      </div>
      <div class="col-md-12">
        <button class="btn btn-success" ng-click="importFile()" ng-disabled="processing">Upload</button>
      </div>
    </div>
  </div>
@endsection

@section('custom_js')
  <script src="/assets/js/directives/fileInput.js"></script>
  <script src="/assets/js/controllers/importCtrl.js"></script>
  <script src="/assets/js/services/customerService.js"></script>
  <script src="/assets/js/services/inventoryService.js"></script>
  <script src="/assets/js/services/zipcodeService.js"></script>
  <script src="/assets/js/services/uploadService.js"></script>
@endsection
