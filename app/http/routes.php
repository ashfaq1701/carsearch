<?php

$router->get('/', 'App\Http\Controllers\FrontController::home');
$router->get('import', 'App\Http\Controllers\FrontController::importer');

$router->post('upload/csv', 'App\Http\Controllers\UploadController::uploadCSVFile');

$router->post('api/customers/import', 'App\Http\Controllers\Api\CustomerController::import');

$router->post('api/inventories/import', 'App\Http\Controllers\Api\InventoryController::import');
$router->get('api/inventories/search', 'App\Http\Controllers\Api\InventoryController::search');

$router->post('api/zipcodes/import', 'App\Http\Controllers\Api\ZipcodeController::import');
$router->get('api/zipcodes/search', 'App\Http\Controllers\Api\ZipcodeController::search');

?>
