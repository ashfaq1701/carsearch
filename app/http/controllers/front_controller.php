<?php

namespace App\Http\Controllers;

use App\Core\Controller;
use App\Core\Request;
use Doctrine\ORM\EntityManager;

class FrontController extends Controller {
  public function __construct()
  {
    parent::__construct();
  }

  public function home(Request $req, EntityManager $entityManager)
  {
    $this->response->render('home');
  }

  public function importer()
  {
    $this->response->render('import');
  }
}

?>
