<?php

namespace App\Http\Controllers;

use App\Core\Controller;
use App\Core\Request;

class UploadController extends Controller {
  public function __construct()
  {
    parent::__construct();
  }

  public function uploadCSVFile(Request $req) {
    $file = $req->file('file');
    $origName = $file['name'];
    $fileinfo = pathinfo($origName);
    $newName = $fileinfo['filename'].'-'.time().'.'.$fileinfo['extension'];
    if(strtolower($fileinfo['extension']) == 'csv') {
      if(move_uploaded_file($file['tmp_name'], uploads_path().'/'.$newName))
      {
        $this->response->json(['status' => true, 'name' => $newName]);
      }
      else
      {
        $this->response->json(['status' => false]);
      }
    }
    else {
      $this->response->json(['status' => false]);
    }
  }
}
