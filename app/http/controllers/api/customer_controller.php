<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller;
use App\Core\Request;
use Doctrine\ORM\EntityManager;
use App\Repositories\CustomerRepository;

class CustomerController extends Controller {
  public $customerRepository;

  public function __construct()
  {
    $this->customerRepository = CustomerRepository::getInstance();
    parent::__construct();
  }

  public function import(Request $req) {
    $filename = $req->getJsonData('filename');
    $filepath = uploads_path().'/'.$filename;
    $res = $this->customerRepository->importCustomers($filepath);
    $this->response->json(['status' => $res]);
  }
}
