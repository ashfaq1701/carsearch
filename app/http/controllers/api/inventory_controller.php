<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller;
use App\Core\Request;
use Doctrine\ORM\EntityManager;
use App\Repositories\InventoryRepository;

class InventoryController extends Controller {
  public $inventoryRepository;

  public function __construct()
  {
    $this->inventoryRepository = InventoryRepository::getInstance();
    parent::__construct();
  }

  public function search(Request $req)
  {
    $zipcodeId = $req->input('zipcode');
    $distance = $req->input('distance');
    $offset = $req->input('start');
    $limit = $req->input('length');
    $offset = (empty($offset) ? 0 : intval($offset));
    $limit = (empty($limit) ? 0 : intval($limit));
    if(empty($zipcodeId) || empty($distance))
    {
      $inventories = [];
      $count = 0;
    }
    else
    {
      $res = $this->inventoryRepository->search($zipcodeId, $distance, $limit, $offset);
      $inventories = $res['inventories'];
      $count = $res['count'];
    }
    $this->response->json([
      'draw' => $req->input('draw'),
      'recordsTotal' => $count,
      'recordsFiltered' => $count,
      'data' => $inventories
    ]);
  }

  public function import(Request $req) {
    $filename = $req->getJsonData('filename');
    $filepath = uploads_path().'/'.$filename;
    $res = $this->inventoryRepository->importInventories($filepath);
    $this->response->json(['status' => $res]);
  }
}
