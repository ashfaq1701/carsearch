<?php

namespace App\Http\Controllers\Api;

use App\Core\Controller;
use App\Core\Request;
use Doctrine\ORM\EntityManager;
use App\Repositories\ZipcodeRepository;

class ZipcodeController extends Controller {
  public $zipcodeRepository;

  public function __construct()
  {
    $this->zipcodeRepository = ZipcodeRepository::getInstance();
    parent::__construct();
  }

  public function search(Request $req)
  {
    $term = $req->input('term');
    $zipcodes = $this->zipcodeRepository->search($term);
    $this->response->json(['zipcodes' => $zipcodes]);
  }

  public function import(Request $req) {
    $filename = $req->getJsonData('filename');
    $filepath = uploads_path().'/'.$filename;
    $res = $this->zipcodeRepository->importZipcodes($filepath);
    $this->response->json(['status' => $res]);
  }
}
