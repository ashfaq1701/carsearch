<?php

namespace App\Repositories;

use App\Core\Repository;
use App\Models\Customer;

class CustomerRepository extends Repository
{
  protected static $instance;

  public function __construct()
  {
    parent::__construct();
    $this->modelName = 'App\Models\Customer';
  }

  public function importCustomers($filepath)
  {
    $file = fopen($filepath,"r");
    $count = -1;
    while(! feof($file))
    {
      $count++;
      $dataRow = fgetcsv($file);
      if($count == 0 && count($dataRow) != 6)
      {
        return false;
      }
      if($count == 0 && !is_numeric($dataRow[0]))
      {
        continue;
      }
      $data = [
        'customerNumber' => $dataRow[0],
        'customerName' => $dataRow[1],
        'address1' => $dataRow[2],
        'ppcPhone' => $dataRow[3],
        'pricingTire' => $dataRow[4],
        'ppcExtension' => $dataRow[5]
      ];
      $existing = $this->selectByData($data);
      if(count($existing) == 0)
      {
        try {
          $customer = new Customer();
          $customer->fromArray($data);
          $this->entityManager->persist($customer);
          $this->entityManager->flush();
        } catch (\Exception $e) {
          continue;
        }
      }
    }
    return true;
  }
}

?>
