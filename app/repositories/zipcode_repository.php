<?php

namespace App\Repositories;

use App\Core\Repository;
use App\Models\Zipcode;

class ZipcodeRepository extends Repository
{
  protected static $instance;

  public function __construct()
  {
    parent::__construct();
    $this->modelName = 'App\Models\Zipcode';
  }

  public function search($term)
  {
    $qb = $this->entityManager->createQueryBuilder()
              ->select('m')
              ->from($this->modelName, 'm')
              ->where("m.zipcode LIKE ?1")
              ->setParameter(1, $term.'%');
    $q = $qb->getQuery();
    return $q->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
  }

  public function getNearbyZipcodes($zipcodeId, $distance)
  {
    $zipcode = $this->entityManager->find($this->modelName, $zipcodeId);
    $qb = $this->entityManager->createQueryBuilder()
              ->select('m', '6371 * acos(
                cos(radians(:lat)) *
                cos(radians(m.latitude)) *
                cos(
                  radians(m.longitude) - radians(:lng)
                ) +
                sin(radians(:lat)) *
                sin(radians(m.latitude)) AS distance')
              ->from($this->modelName, 'm')
              ->having("distance < :distance")
              ->setParameter('lat', $zipcode->getLatitude())
              ->setParameter('lng', $zipcode->getLongitude())
              ->setParameter('distance', $distance);
    $q = $qb->getQuery();
    return $q->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
  }

  public function importZipcodes($filepath)
  {
    $file = fopen($filepath,"r");
    $count = -1;
    while(! feof($file))
    {
      $count++;
      $dataRow = fgetcsv($file);
      if($count == 0 && count($dataRow) != 6)
      {
        return false;
      }
      if($count == 0 && !is_numeric($dataRow[2]))
      {
        continue;
      }
      $data = [
        'zipcode' => $dataRow[0],
        'city' => $dataRow[4],
        'state' => $dataRow[5],
        'stateCode' => $dataRow[1],
        'latitude' => $dataRow[2],
        'longitude' => $dataRow[3]
      ];
      $existing = $this->selectByData($data);
      if(count($existing) == 0)
      {
        try {
          $zipcode = new Zipcode();
          $zipcode->fromArray($data);
          $this->entityManager->persist($zipcode);
          if($count % 100 == 0)
          {
            $this->entityManager->flush();
          }
        } catch (\Exception $e) {
          continue;
        }
      }
    }
    try
    {
      $this->entityManager->flush();
    }
    catch (\Exception $e) {
      return true;
    }
    return true;
  }
}

?>
