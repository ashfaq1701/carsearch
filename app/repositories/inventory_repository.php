<?php

namespace App\Repositories;

use App\Core\Repository;
use App\Repositories\CustomerRepository;
use App\Repositories\ZipcodeRepository;
use App\Models\Inventory;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\Query\ResultSetMapping;

class InventoryRepository extends Repository
{
  protected static $instance;
  protected $customerRepository;
  protected $zipcodeRepository;

  public function __construct()
  {
    parent::__construct();
    $this->modelName = 'App\Models\Inventory';
    $this->customerRepository = CustomerRepository::getInstance();
    $this->zipcodeRepository = ZipcodeRepository::getInstance();
  }

  public function search($zipcodeId, $distance, $limit, $offset)
  {
    $zipcode = $this->entityManager->find('App\Models\Zipcode', $zipcodeId);
    $rsm = new ResultSetMappingBuilder($this->entityManager, ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
    $rsm->addRootEntityFromClassMetadata(\App\Models\Inventory::class, 'a');
    $rsm->addJoinedEntityFromClassMetadata(\App\Models\Zipcode::class, 'u', 'a', 'zipcode', array('id' => 'zipcode_id'));
    $rsm->addJoinedEntityFromClassMetadata(\App\Models\Customer::class, 'c', 'a', 'customer', array('id' => 'customer_id'));
    $query = $this->entityManager->createNativeQuery('SELECT SQL_CALC_FOUND_ROWS ' . $rsm->generateSelectClause() . ' , @lat:=u.latitude as lat, @lng:=u.longitude as lng, ( 6371 * acos( cos( radians( :lat ) ) * cos( radians( @lat ) ) * cos( radians( @lng ) - radians( :lng ) ) + sin(radians( :lat )) * sin(radians(@lat)) ) ) as dist FROM inventories a INNER JOIN zipcodes u ON a.zipcode_id = u.id INNER JOIN customers c ON a.customer_id = c.id HAVING dist < :dist ORDER BY dist LIMIT :limit OFFSET :offset', $rsm);
    $query->setParameter('lat', $zipcode->getLatitude());
    $query->setParameter('lng', $zipcode->getLongitude());
    $query->setParameter('dist', $distance);
    $query->setParameter('limit', $limit);
    $query->setParameter('offset', $offset);
    $inventories = $query->getResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);

    $sql = 'SELECT FOUND_ROWS() AS foundRows';
    $rsm2 = new ResultSetMapping();
    $rsm2->addScalarResult('foundRows', 'foundRows');
    $query2 = $this->entityManager->createNativeQuery($sql, $rsm2);
    $foundRows = $query2->getResult();
    $count = $foundRows[0]['foundRows'];

    return [
      'inventories' => $inventories,
      'count' => $count
    ];
  }

  public function importInventories($filepath)
  {
    $file = fopen($filepath,"r");
    $count = -1;
    while(! feof($file))
    {
      $count++;
      $dataRow = fgetcsv($file);
      if($count == 0 && count($dataRow) != 25)
      {
        return false;
      }
      if($count == 0 && !is_numeric($dataRow[0]))
      {
        continue;
      }
      $zipcode = null;
      $customer = null;
      $zipcodeSearch = [
        'zipcode' => $dataRow[20]
      ];
      $existingZipcodes = $this->zipcodeRepository->selectByData($zipcodeSearch);
      if(count($existingZipcodes) > 0)
      {
        $zipcode = $existingZipcodes[0];
      }

      $customerSearch = [
        'customerNumber' => $dataRow[23]
      ];
      $existingCustomers = $this->customerRepository->selectByData($customerSearch);
      if(count($existingCustomers) > 0)
      {
        $customer = $existingCustomers[0];
      }

      $data = [
        'usedDealerVehicleLiveId' => $dataRow[0],
        'vin' => $dataRow[1],
        'stock' => $dataRow[2],
        'make' => $dataRow[3],
        'model' => $dataRow[4],
        'trim' => $dataRow[5],
        'year' => $dataRow[6],
        'amenities' => $dataRow[7],
        'price' => $dataRow[8],
        'miles' => $dataRow[9],
        'exterior' => $dataRow[10],
        'description' => $dataRow[11],
        'certified' => $dataRow[12],
        'transmission' => $dataRow[13],
        'bodyType' => $dataRow[14],
        'speeds' => $dataRow[15],
        'doors' => $dataRow[16],
        'cylinders' => $dataRow[17],
        'engine' => $dataRow[18],
        'displacement' => $dataRow[19],
        'phone' => $dataRow[21],
        'imagefile' => $dataRow[22],
        'distance' => $dataRow[24]
      ];
      $dataWithAssoc = $data;
      if(!empty($zipcode))
      {
        $dataWithAssoc['zipcodeId'] = $zipcode->getId();
      }
      if(!empty($customer))
      {
        $dataWithAssoc['customerId'] = $customer->getId();
      }
      $existing = $this->selectByData($dataWithAssoc);
      if(count($existing) == 0)
      {
        try {
          $inventory = new Inventory();
          $inventory->fromArray($data);
          if(!empty($zipcode))
          {
            $inventory->setZipcode($zipcode);
          }
          if(!empty($customer))
          {
            $inventory->setCustomer($customer);
          }
          $this->entityManager->persist($inventory);
          if($count % 200 == 0)
          {
            $this->entityManager->flush();
          }
        } catch (\Exception $e) {
          continue;
        }
      }
    }
    try
    {
      $this->entityManager->flush();
    }
    catch (\Exception $e) {
      return true;
    }
    return true;
  }
}

?>
