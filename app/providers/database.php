<?php

namespace App\Providers;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

class Database extends Provider
{
  public function __construct()
  {
    parent::__construct();
  }
  public function boot()
  {
    $dbConf = (require __DIR__.'/../../config/database.php');
    $paths = array(__DIR__.'/../models');
    $isDevMode = false;
    $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, null, null, false);
    $config->setProxyDir(__DIR__.'/../proxies/');
    $config->setProxyNamespace('App\Proxies');
    $config->addCustomNumericFunction('acos', 'DoctrineExtensions\Query\Mysql\Acos');
    $config->addCustomNumericFunction('cos', 'DoctrineExtensions\Query\Mysql\Cos');
    $config->addCustomNumericFunction('radians', 'DoctrineExtensions\Query\Mysql\Radians');
    $config->addCustomNumericFunction('sin', 'DoctrineExtensions\Query\Mysql\Sin');
    $entityManager = EntityManager::create($dbConf, $config);
    $this->container->bind('entityManager', $entityManager);
  }
}

?>
