<?php

namespace App\Models;

use Doctrine\ORM\Mapping as ORM;
use App\Core\Model;

/**
 * Customer
 *
 * @ORM\Table(name="customers")
 * @ORM\Entity
 */
class Customer extends Model
{
    protected $massAssignable = ['customerNumber', 'customerName', 'address1', 'ppcPhone', 'pricingTire', 'ppcExtension'];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_number", type="integer", nullable=false)
     */
    protected $customerNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_name", type="string", length=200, nullable=true)
     */
    protected $customerName;

    /**
     * @var string
     *
     * @ORM\Column(name="address_1", type="string", length=300, nullable=true)
     */
    protected $address1;

    /**
     * @var string
     *
     * @ORM\Column(name="ppc_phone", type="string", length=30, nullable=true)
     */
    protected $ppcPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="pricing_tire", type="string", length=1, nullable=true)
     */
    protected $pricingTire;

    /**
     * @var string
     *
     * @ORM\Column(name="ppc_extension", type="string", length=10, nullable=true)
     */
    protected $ppcExtension;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerNumber
     *
     * @param integer $customerNumber
     *
     * @return Customer
     */
    public function setCustomerNumber($customerNumber)
    {
        $this->customerNumber = $customerNumber;

        return $this;
    }

    /**
     * Get customerNumber
     *
     * @return integer
     */
    public function getCustomerNumber()
    {
        return $this->customerNumber;
    }

    /**
     * Set customerName
     *
     * @param string $customerName
     *
     * @return Customer
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;

        return $this;
    }

    /**
     * Get customerName
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Set address1
     *
     * @param string $address1
     *
     * @return Customer
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set ppcPhone
     *
     * @param string $ppcPhone
     *
     * @return Customer
     */
    public function setPpcPhone($ppcPhone)
    {
        $this->ppcPhone = $ppcPhone;

        return $this;
    }

    /**
     * Get ppcPhone
     *
     * @return string
     */
    public function getPpcPhone()
    {
        return $this->ppcPhone;
    }

    /**
     * Set pricingTire
     *
     * @param string $pricingTire
     *
     * @return Customer
     */
    public function setPricingTire($pricingTire)
    {
        $this->pricingTire = $pricingTire;

        return $this;
    }

    /**
     * Get pricingTire
     *
     * @return string
     */
    public function getPricingTire()
    {
        return $this->pricingTire;
    }

    /**
     * Set ppcExtension
     *
     * @param string $ppcExtension
     *
     * @return Customer
     */
    public function setPpcExtension($ppcExtension)
    {
        $this->ppcExtension = $ppcExtension;

        return $this;
    }

    /**
     * Get ppcExtension
     *
     * @return string
     */
    public function getPpcExtension()
    {
        return $this->ppcExtension;
    }
}
