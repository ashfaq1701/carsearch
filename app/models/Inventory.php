<?php

namespace App\Models;

use Doctrine\ORM\Mapping as ORM;
use App\Core\Model;

/**
 * Inventory
 *
 * @ORM\Table(name="inventories", indexes={@ORM\Index(name="fk_inventories_1_idx", columns={"zipcode_id"}), @ORM\Index(name="fk_inventories_2_idx", columns={"customer_id"})})
 * @ORM\Entity
 */
class Inventory extends Model
{
    protected $massAssignable = ['usedDealerVehicleLiveId', 'vin', 'stock', 'make', 'model', 'trim', 'year', 'amenities', 'price', 'miles', 'exterior', 'description', 'certified', 'transmission', 'bodyType', 'speeds', 'doors', 'cylinders', 'engine', 'displacement', 'phone', 'imagefile', 'distance'];

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="used_dealer_vehicle_live_id", type="text", length=16777215, nullable=true)
     */
    protected $usedDealerVehicleLiveId;

    /**
     * @var string
     *
     * @ORM\Column(name="vin", type="string", length=30, nullable=true)
     */
    protected $vin;

    /**
     * @var string
     *
     * @ORM\Column(name="stock", type="string", length=50, nullable=true)
     */
    protected $stock;

    /**
    * @var string
    *
    * @ORM\Column(name="make", type="string", length=50, nullable=true)
    */
    protected $make;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=100, nullable=true)
     */
    protected $model;

    /**
     * @var string
     *
     * @ORM\Column(name="trim", type="string", length=200, nullable=true)
     */
    protected $trim;

    /**
     * @var string
     *
     * @ORM\Column(name="year", type="string", length=4, nullable=true)
     */
    protected $year;

    /**
     * @var string
     *
     * @ORM\Column(name="amenities", type="text", length=65535, nullable=true)
     */
    protected $amenities;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=true)
     */
    protected $price;

    /**
     * @var string
     *
     * @ORM\Column(name="miles", type="text", length=16777215, nullable=true)
     */
    protected $miles;

    /**
     * @var string
     *
     * @ORM\Column(name="exterior", type="string", length=100, nullable=true)
     */
    protected $exterior;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    protected $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="certified", type="boolean", nullable=true)
     */
    protected $certified;

    /**
     * @var string
     *
     * @ORM\Column(name="transmission", type="string", length=2, nullable=true)
     */
    protected $transmission;

    /**
     * @var string
     *
     * @ORM\Column(name="body_type", type="string", length=100, nullable=true)
     */
    protected $bodyType;

    /**
     * @var float
     *
     * @ORM\Column(name="speeds", type="float", precision=10, scale=0, nullable=true)
     */
    protected $speeds;

    /**
     * @var integer
     *
     * @ORM\Column(name="doors", type="integer", nullable=true)
     */
    protected $doors;

    /**
     * @var integer
     *
     * @ORM\Column(name="cylinders", type="integer", nullable=true)
     */
    protected $cylinders;

    /**
     * @var string
     *
     * @ORM\Column(name="engine", type="string", length=100, nullable=true)
     */
    protected $engine;

    /**
     * @var integer
     *
     * @ORM\Column(name="displacement", type="integer", nullable=true)
     */
    protected $displacement;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="imagefile", type="text", length=65535, nullable=true)
     */
    protected $imagefile;

    /**
     * @var integer
     *
     * @ORM\Column(name="distance", type="integer", nullable=true)
     */
    protected $distance;

    /**
    * @var integer
    *
    * @ORM\Column(name="zipcode_id", type="integer", nullable=true)
    */
    protected $zipcodeId;

    /**
     * @var \App\Models\Zipcode
     *
     * @ORM\ManyToOne(targetEntity="App\Models\Zipcode")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="zipcode_id", referencedColumnName="id")
     * })
     */
    protected $zipcode;

    /**
    * @var integer
    *
    * @ORM\Column(name="customer_id", type="integer", nullable=true)
    */
    protected $customerId;

    /**
     * @var \App\Models\Customer
     *
     * @ORM\ManyToOne(targetEntity="App\Models\Customer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     * })
     */
    protected $customer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set usedDealerVehicleLiveId
     *
     * @param string $usedDealerVehicleLiveId
     *
     * @return Inventory
     */
    public function setUsedDealerVehicleLiveId($usedDealerVehicleLiveId)
    {
        $this->usedDealerVehicleLiveId = $usedDealerVehicleLiveId;

        return $this;
    }

    /**
     * Get usedDealerVehicleLiveId
     *
     * @return string
     */
    public function getUsedDealerVehicleLiveId()
    {
        return $this->usedDealerVehicleLiveId;
    }

    /**
     * Set vin
     *
     * @param string $vin
     *
     * @return Inventory
     */
    public function setVin($vin)
    {
        $this->vin = $vin;

        return $this;
    }

    /**
     * Get vin
     *
     * @return string
     */
    public function getVin()
    {
        return $this->vin;
    }

    /**
     * Set stock
     *
     * @param string $stock
     *
     * @return Inventory
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set make
     *
     * @param string $make
     *
     * @return Inventory
     */
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get make
     *
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return Inventory
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set trim
     *
     * @param string $trim
     *
     * @return Inventory
     */
    public function setTrim($trim)
    {
        $this->trim = $trim;

        return $this;
    }

    /**
     * Get trim
     *
     * @return string
     */
    public function getTrim()
    {
        return $this->trim;
    }

    /**
     * Set year
     *
     * @param string $year
     *
     * @return Inventory
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set amenities
     *
     * @param string $amenities
     *
     * @return Inventory
     */
    public function setAmenities($amenities)
    {
        $this->amenities = $amenities;

        return $this;
    }

    /**
     * Get amenities
     *
     * @return string
     */
    public function getAmenities()
    {
        return $this->amenities;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Inventory
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set miles
     *
     * @param string $miles
     *
     * @return Inventory
     */
    public function setMiles($miles)
    {
        $this->miles = $miles;

        return $this;
    }

    /**
     * Get miles
     *
     * @return string
     */
    public function getMiles()
    {
        return $this->miles;
    }

    /**
     * Set exterior
     *
     * @param string $exterior
     *
     * @return Inventory
     */
    public function setExterior($exterior)
    {
        $this->exterior = $exterior;

        return $this;
    }

    /**
     * Get exterior
     *
     * @return string
     */
    public function getExterior()
    {
        return $this->exterior;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Inventory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set certified
     *
     * @param boolean $certified
     *
     * @return Inventory
     */
    public function setCertified($certified)
    {
        $this->certified = $certified;

        return $this;
    }

    /**
     * Get certified
     *
     * @return boolean
     */
    public function getCertified()
    {
        return $this->certified;
    }

    /**
     * Set transmission
     *
     * @param string $transmission
     *
     * @return Inventory
     */
    public function setTransmission($transmission)
    {
        $this->transmission = $transmission;

        return $this;
    }

    /**
     * Get transmission
     *
     * @return string
     */
    public function getTransmission()
    {
        return $this->transmission;
    }

    /**
     * Set bodyType
     *
     * @param string $bodyType
     *
     * @return Inventory
     */
    public function setBodyType($bodyType)
    {
        $this->bodyType = $bodyType;

        return $this;
    }

    /**
     * Get bodyType
     *
     * @return string
     */
    public function getBodyType()
    {
        return $this->bodyType;
    }

    /**
     * Set speeds
     *
     * @param float $speeds
     *
     * @return Inventory
     */
    public function setSpeeds($speeds)
    {
        $this->speeds = $speeds;

        return $this;
    }

    /**
     * Get speeds
     *
     * @return float
     */
    public function getSpeeds()
    {
        return $this->speeds;
    }

    /**
     * Set doors
     *
     * @param integer $doors
     *
     * @return Inventory
     */
    public function setDoors($doors)
    {
        $this->doors = $doors;

        return $this;
    }

    /**
     * Get doors
     *
     * @return integer
     */
    public function getDoors()
    {
        return $this->doors;
    }

    /**
     * Set cylinders
     *
     * @param integer $cylinders
     *
     * @return Inventory
     */
    public function setCylinders($cylinders)
    {
        $this->cylinders = $cylinders;

        return $this;
    }

    /**
     * Get cylinders
     *
     * @return integer
     */
    public function getCylinders()
    {
        return $this->cylinders;
    }

    /**
     * Set engine
     *
     * @param string $engine
     *
     * @return Inventory
     */
    public function setEngine($engine)
    {
        $this->engine = $engine;

        return $this;
    }

    /**
     * Get engine
     *
     * @return string
     */
    public function getEngine()
    {
        return $this->engine;
    }

    /**
     * Set displacement
     *
     * @param integer $displacement
     *
     * @return Inventory
     */
    public function setDisplacement($displacement)
    {
        $this->displacement = $displacement;

        return $this;
    }

    /**
     * Get displacement
     *
     * @return integer
     */
    public function getDisplacement()
    {
        return $this->displacement;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Inventory
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set imagefile
     *
     * @param string $imagefile
     *
     * @return Inventory
     */
    public function setImagefile($imagefile)
    {
        $this->imagefile = $imagefile;

        return $this;
    }

    /**
     * Get imagefile
     *
     * @return string
     */
    public function getImagefile()
    {
        return $this->imagefile;
    }

    /**
     * Set distance
     *
     * @param integer $distance
     *
     * @return Inventory
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return integer
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set zipcodeId
     *
     * @param integer $zipcodeId
     *
     * @return Inventory
     */
    public function setZipcodeId($zipcodeId)
    {
        $this->zipcodeId = $zipcodeId;

        return $this;
    }

    /**
     * Get zipcodeId
     *
     * @return integer
     */
    public function getZipcodeId()
    {
        return $this->zipcodeId;
    }

    /**
     * Set zipcode
     *
     * @param \App\Models\Zipcode $zipcode
     *
     * @return Inventory
     */
    public function setZipcode(\App\Models\Zipcode $zipcode = null)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return \App\Models\Zipcode
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Set customerId
     *
     * @param integer $customerId
     *
     * @return Inventory
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;

        return $this;
    }

    /**
     * Get customerId
     *
     * @return integer
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * Set customer
     *
     * @param \App\Models\Customer $customer
     *
     * @return Inventory
     */
    public function setCustomer(\App\Models\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \App\Models\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
