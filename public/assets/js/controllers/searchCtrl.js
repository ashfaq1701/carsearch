app.controller('searchCtrl', function($scope, InventoryService, ZipcodeService, DTOptionsBuilder, DTColumnBuilder, DTColumnDefBuilder) {
  $scope.zipcode = null;
  $scope.distance = 0;

  $scope.inventories = [];

  $scope.dtColumnDefs = [
    DTColumnBuilder.newColumn('year').withTitle('Year'),
    DTColumnBuilder.newColumn('make').withTitle('Make'),
    DTColumnBuilder.newColumn('model').withTitle('Model'),
    DTColumnBuilder.newColumn('price').withTitle('Price'),
    DTColumnBuilder.newColumn('customer.customerName').withTitle('Dealership Name'),
    DTColumnBuilder.newColumn('zipcode.state').withTitle('State'),
    DTColumnBuilder.newColumn('zipcode.city').withTitle('City'),
    DTColumnBuilder.newColumn('zipcode.zipcode').withTitle('Zipcode')
  ];

  $scope.dtInstance = {};

  $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('ajax', {
    url: '/api/inventories/search',
    type: 'GET',
    dataSrc: 'data',
  })
  .withOption('fnServerParams', function (aoData) {
    aoData.zipcode = $scope.zipcode;
    aoData.distance = $scope.distance;
  })
  .withOption('processing', true)
  .withOption('serverSide', true)
  .withPaginationType('full_numbers');

  $scope.zipcodeOptions = [];
  $scope.zipcodeConfig = {
    delimiter: ',',
    persist: false,
    valueField: 'id',
    labelField: 'zipcode',
    searchField: 'zipcode',
    maxItems: 1,
    render: {
    	option: function(item, escape) {
        return '<div data-value="'+item.id+'">'+item.zipcode+" - "+item.city+' ('+item.stateCode+')'+'</div>';
      }
    },
    load: function(query, callback) {
    	if (!query.length) return callback();
      ZipcodeService.searchZipcodes(query).then(function(resp)
      {
        var data = resp.data;
        var zipcodes = data.zipcodes;
        callback(zipcodes);
      }, function(resp)
      {
        callback();
      });
    }
  };

  $scope.fetchInventories = function()
  {
    $scope.dtInstance.rerender();
  }
});
