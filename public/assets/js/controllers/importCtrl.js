app.controller('importCtrl', function($scope, UploadService, ZipcodeService, CustomerService, InventoryService) {
  $scope.uploadFile = null;
  $scope.uploadType = 'zipcode';
  $scope.uploadedName = null;
  $scope.processing = false;

  $scope.fileUploaded = function()
  {
    $scope.processing = true;
    UploadService.uploadFile($scope.uploadFile).then(function(resp)
    {
      var data = resp.data;
      data = JSON.parse(data);
      if(data.status == true)
      {
        $scope.uploadedName = data.name;
      }
      $scope.processing = false;
    }, function()
    {
      $scope.processing = false;
    });
  }

  $scope.importFile = function()
  {
    if($scope.uploadedName == null) {
      alert('No file got uploaded');
    }
    else {
      $scope.processing = true;
      var serviceCall = null;
      if($scope.uploadType == 'zipcode')
      {
        serviceCall = ZipcodeService.importZipcodes($scope.uploadedName);
      }
      else if($scope.uploadType == 'customer')
      {
        serviceCall = CustomerService.importCustomers($scope.uploadedName);
      }
      else
      {
        serviceCall = InventoryService.importInventories($scope.uploadedName);
      }
      serviceCall.then(function(resp)
      {
        $scope.processing = false;
        var data = resp.data;
        //data = JSON.parse(data);
        if(data.status == true) {
          alert('Entities in file got imported');
        }
        else {
          alert('Could not import entities from the file');
        }
      }, function(resp)
      {
        $scope.processing = false;
        alert('Could not import entities from the file');
      });
    }
  }
});
