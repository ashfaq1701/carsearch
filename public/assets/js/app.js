var app = angular.module("carSearchApp", ['selectize', 'datatables', 'angularSpinner'], function($interpolateProvider){
	$interpolateProvider.startSymbol('[[').endSymbol(']]');
});
