app.factory('InventoryService', ['$http', function($http)
{
  return {
    importInventories: function(file) {
      return $http.post('/api/inventories/import', { filename: file });
    },
    searchInventories: function(zipcode, distance) {
      return $http.get('/api/inventories/search', { params: { zipcode: zipcode, distance: distance } });
    }
  };
}]);
