app.factory('UploadService', ['$http', function($http)
{
  return {
    uploadFile: function(file)
    {
      var config = {
        headers: {
          'Content-Type': undefined
        },
        transformResponse: angular.identity
      };
      var fd = new FormData();
      fd.append('file', file);
      return $http.post('/upload/csv', fd, config);
    }
  };
}]);
