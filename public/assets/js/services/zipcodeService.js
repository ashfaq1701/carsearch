app.factory('ZipcodeService', ['$http', function($http)
{
  return {
    importZipcodes: function(file) {
      return $http.post('/api/zipcodes/import', { filename: file });
    },
    searchZipcodes: function(term) {
      return $http.get('/api/zipcodes/search', { params: { term: term } });
    }
  };
}]);
