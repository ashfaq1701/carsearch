app.factory('CustomerService', ['$http', function($http)
{
  return {
    importCustomers: function(file) {
      return $http.post('/api/customers/import', { filename: file });
    }
  };
}]);
